#!/bin/bash

FOLDER=$(date +"%Y-%b-%d_%H-%M-%S")
GREEN='\033[0;32m'
NC='\033[0m'

for i in `docker volume ls -f name=miko -q`; do
	echo -e "${GREEN}Tar $i in progress"
	docker run --rm -v $i:/volume -v /tmp/backup/$FOLDER:/backup alpine tar -cjf /backup/$i.tar.bz2 -C /volume ./
done

echo -e "${GREEN}Build ftp-backup${NC}"
docker build -t ftp-backup ./ftp-backup 

echo -e "${GREEN}Push FTP into --> dedibackup${NC}"
docker run --rm -v /tmp/backup/:/backup:ro ftp-backup lftp -e "mirror -R /backup / && exit" -d -u USER,PASSWORD HOSTNAME

if [-e /tmp/backup/${FOLDER}/${i}.tar.bz2]; then
	rm -rfv /tmp/backup > /var/log/backup/rm
fi

echo -e "\n\n\n\n"
exit 0
