#!/bin/bash
# Sauvegarde des bases PostgreSQL du serveur
# Définitions des variables
backupdir="/home/postgre-backup/"
datejour=`date +%Y%m%d`

# Vu qu'on envoi l'archive sur un serveur distant, on définie quelques variables de plus
remoteserver="mon.serveur.ftp.com 21" 
remotepath=/path/vers/postgres-backup
username="backup-user"
password="Mon super mot 2 passe ultra secure écrit en clair dans le script!"
# Juste au cas ou, n'oubliez pas de chmod 700 le script et de le mettre dans un endroit inaccessible au public ^_^;

# On affiche les infos date du jour pour le log
echo "date du jour : $datejour"

# On supprime les anciennes archives par souci d'espace disque
rm -f $backupdir/*.tar.gz

# On sauvegarde chaque base dans un sql different :
databases=`psql -l -t | cut -d'|' -f1 | sed -e 's/ //g' -e '/^$/d'`
for i in $databases; do
  if [ "$i" != "template0" ] && [ "$i" != "template1" ]; then
    echo Sauvegarde $i dans $backupdir$i\_$datejour.sql
    pg_dump -Fc $i > $backupdir$i\_$datejour.sql
  fi
done
echo "Dump OK, début de la compression..."
# On archive tous les backup a la date du jour
tar -czf $datejour-SqlBackup.tar.gz *.sql
echo "Terminée, on bazarde le .taz.gz sur un serveur distant..."

# Le fichier du jour est envoyé sur le serveur, au choix:

# rsync si présent et si le certificat de connexion est installé pour l'utilisateur du script
#rsync -a -e ssh $backupdir $username@$remoteserver:$remotepath/

# Si LFTP est installé sur le serveur
#lftp -u $username,$password -e "mirror --reverse --verbose $backupdir $remotepath" $remoteserver << bye

# Sinon, du simple FTP
ftp -inv <<EOF
  open $remoteserver
  user $username "$password"
  prompt
  cd $remotepath
  put $datejour-SqlBackup.tar.gz
  bye
EOF

# On supprime les restes des backup dans le dossier d'éxécution
rm -f *.sql
echo "Terminé"
echo "-----------------------------------------------------------"
