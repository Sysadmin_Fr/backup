﻿#Le "If Test-Path", vérifie s'il y a des fichiers du format que l'on veut copier depuis le dossier source
#Pour un Test-Path sur un lecteur, on ne mettra pas l'astérisque. Exemple : Test-Path -Path ""B:\"
If (Test-Path -Path "B:\Desktop\SourcesBackup\*" -Include *.bmp, *.docx, *.txt, *.msg, *.xlsx, *.vbs, *.pdf) 
    {

#Créer un nouveau dossier avec en nom la date et l'heure lors du backup
New-Item -ItemType Directory -Path "B:\Desktop\DestinationBackup\$((Get-Date).ToString('yyyy-MM-dd_hh-mm'))"

#Déplace les fichiers du dossier source vers le dossier de destination créé précédemment et fait un fichier texte récapitulatif
Get-ChildItem -Path "B:\Desktop\SourcesBackup\*" -Include *.bmp, *.docx, *.txt, *.msg, *.xlsx, *.vbs, *.pdf -Recurse | Copy-Item -Destination "B:\Desktop\DestinationBackup\$((Get-Date).ToString('yyyy-MM-dd_hh-mm'))" -Force -PassThru | Out-File B:\Desktop\DestinationBackup\Copy.txt

# /!\ Supprime les fichiers sources une fois les fichiers copiés vers la destination /!\
Get-ChildItem -Path "B:\Desktop\SourcesBackup\*" -Include *.bmp, *.docx, *.txt, *.msg, *.xlsx, *.vbs, *.pdf -Recurse | foreach { Remove-Item -Path $_.FullName }

#Se déplace dans le répertoire de destination pour la rétention des 8 objets les plus récents
cd "B:\Desktop\DestinationBackup\"

#Il sélectionne les 8 premiers objets les plus récents et supprime le reste
Get-ChildItem B:\Desktop\DestinationBackup\ | Sort-Object LastWriteTime -Descending | Select-Object -Skip 8 | Remove-Item -Force -Recurse
    }   
     Else{Exit}
